<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="https://use.typekit.net/cfp1omk.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<header<?php if(is_page_template('homepage.php')): ?> class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);"	<?php endif; ?>>
		<div class="content">
			<div class="wrapper">
						
				<?php get_template_part('partials/header/social'); ?>

				<?php get_template_part('partials/header/logo'); ?>

				<?php get_template_part('partials/header/nav-toggle'); ?>

				<?php if(is_page_template('homepage.php')): ?>

					<div class="home-headline">
						<h1><?php echo get_field('hero_headline'); ?></h1>
					</div>

					<div class="copy">
						<?php echo get_field('about_statement'); ?>
					</div>

				<?php endif; ?>

			</div>
		</div>
	</header>

	<nav>
		<div class="wrapper">
			<div class="nav-wrapper">
				
				<div class="links">
					<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
					 	
					 	<div class="link">
						    <a href="<?php echo get_sub_field('link'); ?>">
						        <?php echo get_sub_field('label'); ?>
						    </a>
					 	</div>

					<?php endwhile; endif; ?>
				</div>

			</div>
		</div>
	</nav>