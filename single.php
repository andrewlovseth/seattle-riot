<?php get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    	<section class="page-header">
    		<div class="wrapper">
				<div class="date">
					<h4><?php the_time('j M Y'); ?></h4>
				</div>

				<div class="headline">
					<h1><?php the_title(); ?></h1>
				</div>

    			<div class="author">
					<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
				
						<h3>
							by <?php the_title(); ?><?php if(get_field('jersey_number')): ?>, #<?php echo get_field('jersey_number'); ?><?php endif; ?>
						</h3>    

					<?php wp_reset_postdata(); endif; ?>			
    			</div>
    		</div>
    	</section>    

		<article class="copy p2 extended">
			<div class="wrapper">

				<?php the_content(); ?>  
		   
		   </div>
		</article>
        
    <?php endwhile; endif; ?>

<?php get_footer(); ?>