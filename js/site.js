$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){
		$('header, nav, body').toggleClass('open');
		$('nav').fadeToggle(200);
		return false;
	});


	var $wrapper = $('.single-opponents .games');

	$wrapper.find('.game').sort(function (a, b) {
	    return +b.getAttribute('data-sort') - +a.getAttribute('data-sort');
	})
	.appendTo( $wrapper );


	// Home Hero Slider
	let slide = $('body.home .tabs');
	slide.slick({
		arrows: false,
		dots: false,
		adaptiveHeight: true,
		draggable: false
	});

	$('.tab-links .tab-link a').click(function(e){
	   e.preventDefault();
	   slideIndex = $(this).parent('.tab-link').index();
	   slide.slick('slickGoTo', +slideIndex);

	   $('.tab-link a').removeClass('active');
	   $(this).addClass('active');
	});



	// Video Overlay Show
	$('.video-trigger').click(function(){
		var videoID = $(this).data('video-id');
		console.log(videoID);

		$('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" allowfullscreen frameborder="0" allowTransparency="true" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1& modestbranding=1&rel=0"></iframe>');
		$('.video-frame').fitVids();
		$('#video-overlay').fadeIn(200);
		$('body').addClass('video-overlay-open');



		return false;
	});


	// Video Overlay Hide
	$('.video-close-btn').click(function(){
		$('#video-overlay').fadeOut(200);
		$('body').removeClass('video-overlay-open');
		$('#video-overlay .video-frame .content').empty();

		return false;
	});



		
});



// Hide Overlay on ESC
$(document).on('keydown', function(e) {
    if ( e.keyCode === 27 ) {
		$('#video-overlay').fadeOut(200);
		$('body').removeClass('video-overlay-open');
		$('#video-overlay .video-frame .content').empty();
    }
});



// poster frame click event
$(document).on('click','.js-videoPoster',function(ev) {
  ev.preventDefault();
  var $poster = $(this);
  var $wrapper = $poster.closest('.js-videoWrapper');
  videoPlay($wrapper);
});

// play the targeted video (and hide the poster frame)
function videoPlay($wrapper) {
  var $iframe = $wrapper.find('.js-videoIframe');
  var src = $iframe.data('src');
  // hide poster
  $wrapper.addClass('videoWrapperActive');
  // add iframe src in, starting the video
  $iframe.attr('src',src);
}

// stop the targeted/all videos (and re-instate the poster frames)
function videoStop($wrapper) {
  // if we're stopping all videos on page
  if (!$wrapper) {
    var $wrapper = $('.js-videoWrapper');
    var $iframe = $('.js-videoIframe');
  // if we're stopping a particular video
  } else {
    var $iframe = $wrapper.find('.js-videoIframe');
  }
  // reveal poster
  $wrapper.removeClass('videoWrapperActive');
  // remove youtube link, stopping the video from playing in the background
  $iframe.attr('src','');
}