<div class="social">
	<a href="<?php echo get_field('facebook', 'options'); ?>" class="facebook" rel="external">Facebook</a>
	<a href="<?php echo get_field('twitter', 'options'); ?>" class="twitter" rel="external">Twitter</a>
	<a href="<?php echo get_field('instagram', 'options'); ?>" class="instagram" rel="external">Instagram</a>
</div>