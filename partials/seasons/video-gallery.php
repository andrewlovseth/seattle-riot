<?php
	$year = get_field('year');
	$args = array(
		'year' => $year,
		'posts_per_page' => 20,
		'post_type' => 'video'
	);
	$wp_query = new WP_Query( $args );
	if ( $wp_query->have_posts() ) : ?>


	<section id="videos" class="video-archive">
		<div class="wrapper">

			<h3>Videos</h3>

			<?php  while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>          
		    		
		    	<?php include('inc/video.php'); ?>

			<?php endwhile; ?>

		</div>
	</section>

<?php endif; wp_reset_query(); ?>	
