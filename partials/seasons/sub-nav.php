
<section class="sub-nav year-<?php echo get_the_title(); ?>">
	<div class="wrapper">
		
		<?php
			$args = array(
				'post_type' => 'seasons',
				'posts_per_page' => 50,
				'orderby' => 'name',
				'order' => 'ASC'
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : ?>

			<div class="season-wrapper">
	
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>

					<div class="season season-<?php echo get_the_title(); ?>">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>					
					</div>			

				<?php endwhile; ?>
	
			</div>

		<?php endif; wp_reset_postdata(); ?>

	</div>
</section>

