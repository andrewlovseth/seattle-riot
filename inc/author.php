<?php if( $post->ID === 1294 ): ?>
	
	<h3 class="riot">by <?php the_title(); ?></h3>		

<?php else: ?>
					
	<a href="<?php the_permalink(); ?>"><img class="headshot" src="<?php $headshot = get_field('headshot'); echo $headshot['url']; ?>" alt="<?php echo $headshot['alt']; ?>" /></a>

	
	<?php if (get_post_status($post->ID) == 'draft'): ?>

		<h3>
			by <?php the_title(); ?><?php if(get_field('jersey_number')): ?>, #<?php echo get_field('jersey_number'); ?><?php endif; ?>
		</h3>
	
	<?php else: ?>
	
		<h3>
			<a href="<?php the_permalink(); ?>">
				by <?php the_title(); ?><?php if(get_field('jersey_number')): ?>, #<?php echo get_field('jersey_number'); ?><?php endif; ?>
			</a>
		</h3>
	
	<?php endif; ?>
	


<?php endif; ?>