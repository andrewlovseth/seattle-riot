<section class="tournament">
	<div class="wrapper">

		<section class="info">
			<h2><?php echo get_field('tournament_display_name'); ?></h2>
			<h3><?php echo get_field('date'); ?> | <?php echo get_field('location'); ?></h3>
			<?php if(get_field('finish')): ?>
				<h4>Finish: <strong><?php echo get_field('finish'); ?></strong></h4>
			<?php endif; ?>
		</section>
		
		<?php if(have_rows('games')): ?>
			<section class="games">
			
				<?php while(have_rows('games')): the_row(); ?>
					    <div class="game">
					    
					       	<div class="opponent">
							    <?php $post_object = get_sub_field('opponent'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								<?php wp_reset_postdata(); endif; ?> 	
					       	</div>
		
					    	<div class="score">
					    		<span class="riot"><?php echo get_sub_field('riot_score'); ?></span><span class="sep">-</span><span class="opponent"><?php echo get_sub_field('opponent_score'); ?></span>
					    	</div>
		
					    	<div class="type">
						    	<?php echo get_sub_field('game_type'); ?>
					    	</div>
					    	
						</div>
	
				<?php endwhile; ?>
			
			</section>
		<?php endif; ?>

	</div>
</section>