	<div class="article-header">
		<h4><?php the_time('F j, Y'); ?></h4>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		
		<div class="author">
		
			<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
			
				<?php include('author.php'); ?>

			<?php wp_reset_postdata(); endif; ?>
			
		</div>

	</div>