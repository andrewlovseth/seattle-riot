    <article>
		<div class="thumbnail">
			<a href="#video" data-video-src="<?php the_permalink(); ?>">
				<div class="overlay">
					<h4><?php the_title(); ?></h4>
				</div>
				<img src="<?php $image = get_field('video_thumbnail', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>
	</article>
