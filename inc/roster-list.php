<aside class="roster-list">
    <div class="wrapper">

        <h4><?php echo get_field('roster_headline', 'options'); ?></h4>

        <?php $posts = get_field('roster_list', 'options'); if( $posts ): ?>
            <ul>
                <?php foreach( $posts as $post): setup_postdata($post); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <?php if(!get_field('non_player')): ?>
                                <?php if(get_field('jersey_number')): ?>
                                    <span class="jersey-number">#<?php echo get_field('jersey_number'); ?></span>
                                <?php endif; ?>
                            <?php endif; ?>
                            <span class="name">
                                <?php the_title(); ?>
                                <?php if(get_field('captain')): ?>
                                    <span class="captain">(C)</span>
                                <?php endif; ?>
                            </span>
                            <?php if(get_field('non_player')): ?>
                                <span class="jersey-number">(<?php echo get_field('title'); ?>)</span>
                            <?php endif; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
        
    </div>
</aside>