	<footer>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('home_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="footer-nav">
				<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
				 	
				 	<div class="link">
					    <a href="<?php echo get_sub_field('link'); ?>">
					        <?php echo get_sub_field('label'); ?>
					    </a>
				 	</div>

				<?php endwhile; endif; ?>
			</div>

			<?php get_template_part('partials/header/social'); ?>

		</div>
	</footer>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>