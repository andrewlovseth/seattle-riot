<?php get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<h1>Video</h1>

		</div>
	</section>

	<?php if(have_rows('video_gallery', 'options')): while(have_rows('video_gallery', 'options')) : the_row(); ?>
 	    <?php if( get_row_layout() == 'section' ): ?>
			

			<section class="gallery">
				<div class="wrapper">

					<div class="headline section-header">
						<h2><?php echo get_sub_field('section_headline'); ?></h2>
					</div>

					<div class="gallery-wrapper">
						<?php $posts = get_sub_field('videos'); if( $posts ): ?>
							<?php foreach( $posts as $p ): ?>

								<?php if(have_rows('source', $p->ID)): while(have_rows('source', $p->ID)) : the_row(); ?>
								 
								    <?php if( get_row_layout() == 'youtube' ): ?>
										<?php
											$video_source = 'youtube';
											$video_id = get_sub_field('id');
										?>
								    <?php endif; ?>
								 
								<?php endwhile; endif; ?>

								<div class="video">
									<a href="#" class="video-trigger" data-video-id="<?php echo $video_id; ?>" data-video-source="<?php echo $video_source; ?>">
										<div class="image">
											<img src="<?php $image = get_field('thumbnail', $p->ID); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
										</div>

										<div class="info">
											<h4><?php echo get_the_title( $p->ID ); ?></h4>
										</div>
									</a>
								</div>

							<?php endforeach; ?>
						<?php endif; ?>

					</div>
		
				</div>
			</section>

	    <?php endif; ?>	 
	<?php endwhile; endif; ?>

	<section id="video-overlay">
		<div class="overlay">
			<div class="overlay-wrapper">

				<div class="info">
					<div class="close">
						<a href="#" class="video-close-btn"></a>
					</div>

					<div class="video-frame">
						<div class="content">
						    
						</div>				    	
					</div>
				</div>
				
			</div>
		</div>
	</section>

<?php get_footer(); ?>