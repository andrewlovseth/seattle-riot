<?php


/*
	Template Name: Seasons
*/

get_header(); ?>

	
	<section class="page-header">
		<div class="wrapper">
				
			<h1><?php echo get_field('year'); ?></h1>

		</div>
	</section>


	<?php get_template_part('partials/seasons/sub-nav'); ?>


	<section id="season">
		<div class="wrapper">

			<div id="tournament-wrapper">

				<?php
					$year = get_field('year');
					$args = array(
						'year' => $year,
						'posts_per_page' => 20,
						'post_type' => 'results',
						'post_status' => array('future', 'publish')
					);
					$wp_query = new WP_Query( $args );
					if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>          
			        		
			        	<?php get_template_part('partials/seasons/tournament'); ?>
				
				<?php endwhile; endif; wp_reset_query(); ?>	
			</div>

			<aside>
				<h4 class="title">Roster</h4>
				<?php echo get_field('roster'); ?>
			</aside>

		</div>
	</section>


<?php get_footer(); ?>