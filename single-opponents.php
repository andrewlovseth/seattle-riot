<?php get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
				
			<h1><?php the_title();?></h1>

		</div>
	</section>

	<section id="team">
		<div class="wrapper">

			<section class="games">

				<?php
			        $rows = $wpdb->get_results($wpdb->prepare( 
			            "
			            SELECT * 
			            FROM wp_postmeta
			            WHERE meta_key LIKE %s AND meta_value = %s 
			           	ORDER BY meta_value = %s DESC
			            ",
			            'games_%_opponent',
			            $post->ID,
			            'games_%_date'
			        ));
			    ?>

				<?php if( $rows ): ?>
					<?php 


					$wins = 0;
					$losses = 0;

					foreach( $rows as $row ): ?>

						<?php
				
							preg_match('_([0-9]+)_', $row->meta_key, $matches);

							$riot_score_meta_key = 'games_' . $matches[0] . '_riot_score';
							$opponent_score_meta_key = 'games_' . $matches[0] . '_opponent_score';
							$date_meta_key = 'games_' . $matches[0] . '_date';
							$type_meta_key = 'games_' . $matches[0] . '_game_type';

							$riot_score = get_post_meta( $row->post_id, $riot_score_meta_key, true );
							$opponent_score = get_post_meta( $row->post_id, $opponent_score_meta_key, true );
							$date = new DateTime(get_post_meta( $row->post_id, $date_meta_key, true ));
							$type = get_post_meta( $row->post_id, $type_meta_key, true );

							if($riot_score < $opponent_score) { $losses ++; } else { $wins ++; }

						?>

					    <div class="game" data-sort="<?php echo date_format($date, 'Ymd'); ?>">

							<div class="date">
								<span><strong><?php echo get_field('tournament_display_name', $row->post_id ); ?></strong> (<?php echo $type; ?>)</span>
							</div>

					       	<?php
						       	if($riot_score > $opponent_score) {
						       		$result = 'win';
						       	} else {
						       		$result = 'loss';
						       	}
						    ?>

			    			<div class="score <?php echo $result; ?>">
					    		<span class="riot"><?php echo $riot_score; ?></span><span class="sep">-</span><span class="opponent"><?php echo $opponent_score; ?></span>
					    	</div>


							<div class="tournament">
								<span><?php echo date_format($date, 'F j, Y'); ?></span>
							</div>




					    </div>

					<?php endforeach; ?>


				<?php endif; ?>

			</section>

			<section class="win-loss">
				<h3>Overall Record</h3>
				<h2><?php echo $wins; ?> - <?php echo $losses;?></h2>

				<div class="note">
					<p>Based on records we have.</p>
				</div>
				
			</section>	


			</div>
		</section>
        

<?php get_footer(); ?>