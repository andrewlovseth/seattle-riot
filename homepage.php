<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section class="video">
		<div class="wrapper">
			
			<div class="videoWrapper js-videoWrapper">
				<iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowTransparency="true" allowfullscreen data-src="https://www.youtube.com/embed/<?php echo get_field('featured_video_id'); ?>?autoplay=1& modestbranding=1&rel=0"></iframe>
				<a class="videoPoster js-videoPoster" style="background-image:url(<?php $image = get_field('featured_video_poster'); echo $image['url']; ?>);" href="#">Play video</a>
			</div>

			<div class="caption">
				<p><?php echo get_field('featured_video_caption'); ?></p>
			</div>

		</div>
	</section>


	<section class="events">
		<div class="wrapper">

			<div class="section-header">
				<h2><?php echo get_field('events_headline'); ?></h2>
			</div>

			<?php $this_month = date('n'); ?>

			<div class="events-tabs">
				<?php if(have_rows('events_calendar')): $total = count(get_field('events_calendar')); $count = 1; ?>
			     	<div class="tab-links">
			     		<?php while(have_rows('events_calendar')) : the_row(); ?>
						    <?php if( get_row_layout() == 'month' ): ?>

						    	<?php
						    		$active = false;
						    		$month_name = get_sub_field('month_name');
						    		$month_num = date('n', strtotime($month_name));

						    		if($this_month === $month_num) {
						    			$active = true;
						    		} elseif ($this_month < $month_num) {
						    			if($count == 1) {
						    				$active = true;
						    			}
						    		} elseif ($this_month > $month_num) {
						    			if($count == $total) {
						    				$active = true;
						    			}
						    		}
						    	?>
								
								<div class="tab-link">
						    		<a href="#" class="<?php if ($active == true): ?>active<?php endif; ?>"><?php echo get_sub_field('month_name'); ?></a>
								</div>
								
						    <?php $count++; endif; ?>
					    <?php endwhile; ?>
			    	</div>	 	
			 	<?php endif; ?>

				<?php if(have_rows('events_calendar')): ?>
			     	<div class="tabs">
			     		<?php while(have_rows('events_calendar')) : the_row(); ?>
						    <?php if( get_row_layout() == 'month' ): ?>
								
								<div class="tab">
						    		
									<?php if(have_rows('events')): while(have_rows('events')): the_row(); ?>
									 
									    <div class="event">
									    	<div class="headline">
									    		<h3><?php echo get_sub_field('title'); ?></h3>
									    		<?php if(get_sub_field('date')): ?>
									    			<h4>
									    				<?php echo get_sub_field('date'); ?>
									    				<?php if(get_sub_field('location')): ?> | <?php echo get_sub_field('location'); ?><?php endif; ?>
									    			</h4>
									    		<?php endif; ?>
									    	</div>
									    	
									    	<div class="copy">
									    		<?php echo get_sub_field('description'); ?>
									    	</div>
									    </div>

									<?php endwhile; endif; ?>

								</div>
								
						    <?php endif; ?>
						<?php endwhile; ?>
			    	</div>
			 	
			 	<?php endif; ?>
			</div>

		</div>
	</section>


	<section class="latest">
		<div class="wrapper">

			<div class="section-header">
				<h2>Blog</h2>
			</div>

			<div class="posts-wrapper">

				<?php
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 3
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<article class="post latest-post">
						<a href="<?php the_permalink(); ?>">
							<div class="meta">
								<h4><?php the_time('j M Y'); ?></h4>
							</div>
							
							<div class="headline">
								<h3><?php the_title(); ?></h3>	
							</div>

			    			<div class="author">
								<?php $author = get_field('author'); if( $author ): ?>
							
									<h4>
										by <?php echo get_the_title($author->ID); ?><?php if(get_field('jersey_number', $author->ID)): ?>, #<?php echo get_field('jersey_number', $author->ID); ?><?php endif; ?>
									</h4>    

								<?php endif; ?>			
			    			</div>

			    			<div class="teaser">
			    				<?php echo get_field('teaser'); ?>
			    			</div>
						</a>
					</article>

				<?php endwhile; endif; wp_reset_postdata(); ?>

				
			</div>

			<div class="more">
				<a href="<?php echo site_url('/blog'); ?>">Read more posts on our blog</a>
			</div>
		
		</div>
	</section>

<?php get_footer(); ?>