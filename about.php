<?php

/*

	Template Name: About

*/

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<h1><?php echo get_field('hero_headline'); ?></h1>

		</div>
	</section>

	<section class="core-values">
		<div class="wrapper">

			<div class="headline">
				<h3><?php echo get_field('care_values_headline'); ?></h3>
			</div>

			<div class="copy">
				<?php echo get_field('core_values'); ?>
			</div>

		</div>
	</section>

	<section class="general">
		<div class="wrapper">

			<section class="story">
				<div class="headline">
					<h3><?php echo get_field('story_headline'); ?></h3>
				</div>

				<div class="copy p2">
					<?php echo get_field('story'); ?>
				</div>
			</section>

			<section class="results">
				<div class="tournament">
					<h5>USA Nationals</h5>
					<?php if(get_field('national_results')): while(has_sub_field('national_results')): ?>
					 
					    <article<?php if(get_sub_field('finish') == "1st"): ?> class="first"<?php endif; ?>>
					    	<p class="year"><?php echo get_sub_field('year'); ?></p>
					    	<p class="finish"><?php echo get_sub_field('finish'); ?></p>
					    </article>

					<?php endwhile; endif; ?>
				</div>

				<div class="tournament">
					<h5>World Championships</h5>
					<?php if(get_field('worlds_results')): while(has_sub_field('worlds_results')): ?>
					 
					    <article<?php if(get_sub_field('finish') == "1st"): ?> class="first"<?php endif; ?>>
					    	<p class="year"><?php echo get_sub_field('year'); ?></p>
					    	<p class="finish"><?php echo get_sub_field('finish'); ?></p>
					    </article>

					<?php endwhile; endif; ?>
				</div>
			</section>

		</div>
	</section>

<?php get_footer(); ?>