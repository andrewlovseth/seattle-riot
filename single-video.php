<?php get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


		<section id="video-player">
			<div class="wrapper">


				<div id="video-info">

					<h3><?php the_title(); ?></h3>

					<div class="video-player">
						<?php if(have_rows('source')): while(have_rows('source')) : the_row(); ?>
						 
						    <?php if( get_row_layout() == 'youtube' ): ?>

						    	<iframe src="http://www.youtube-nocookie.com/embed/<?php echo get_sub_field('id'); ?>?rel=0&showinfo=0&modestbranding=1&vq=hd1080&autoplay=1" frameborder="0" width="1920" height="1080" allowfullscreen></iframe>
								
						    <?php endif; ?>
						 
						    <?php if( get_row_layout() == 'vimeo' ): ?>

						    	<iframe src="http://player.vimeo.com/video/<?php echo get_sub_field('id'); ?>?title=0&color=a30034&autoplay=1&byline=0" frameborder="0" width="1920" height="1080"></iframe>      
								
						    <?php endif; ?>
						 
						<?php endwhile; endif; ?>
					</div>
				</div>

			</div>
		</section>
        
    <?php endwhile; endif; ?>

<?php get_footer(); ?>