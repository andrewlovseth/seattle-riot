<?php

/*

	Template Name: Sponsors

*/

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
				
			<h1><?php echo get_field('hero_headline'); ?></h1>

		</div>
	</section>


	<section id="list">
		<div class="wrapper">

			<?php if(get_field('sponsors')): while(has_sub_field('sponsors')): ?>
 
			    <div class="sponsor">

			    	<?php if(get_sub_field('link')): ?>

			    		<a href="<?php echo get_sub_field('link'); ?>"><img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
				    
				    <?php else: ?>

				    	<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

				    <?php endif; ?>
			        
			    </div>

			<?php endwhile; endif; ?>

		</div>
	</section>


	<section id="info">
		<div class="wrapper">

			<div class="copy p2">
				<?php echo get_field('sponsor_info'); ?>
			</div>

		</div>
	</section>


<?php get_footer(); ?>