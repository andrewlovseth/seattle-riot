<?php get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
				
			<h1><?php echo get_field('roster_headline', 'options'); ?></h1>

		</div>
	</section>

	<section id="list">
		<div class="wrapper">

	        <?php $posts = get_field('roster_list', 'options'); if( $posts ): ?>
                <?php foreach( $posts as $post): setup_postdata($post); ?>

                	<div class="player">
                		
                		<?php if(get_field('headshot')): ?>
	                		<div class="headshot">
	                			<img src="<?php $image = get_field('headshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	                		</div>

                		<?php endif; ?>

                		<div class="info">
	                		<p>
				        		<?php the_title(); ?> 
				        		<?php if(!get_field('non_player')): ?>
				        			<span class="jersey-number">#<?php echo get_field('jersey_number'); ?></span>
				        		<?php endif; ?>

				        		<?php if(get_field('captain')): ?>
				        			<span class="title">Captain</span>
				        		<?php endif; ?>
	
				        		<?php if(get_field('title')): ?>
				        			<span class="title"><?php echo get_field('title'); ?></span>
				        		<?php endif; ?>
	                        </p>
                    	</div>

                	</div>

                <?php endforeach; wp_reset_postdata(); ?>
	        <?php endif; ?>

		</div>
	</section>

<?php get_footer(); ?>