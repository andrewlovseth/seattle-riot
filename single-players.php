<?php get_header(); ?>


	<section class="profile">
		<div class="wrapper">

			<section class="page-header">
	        	<h1>
	        		<?php the_title(); ?> 
	        		<?php $jerseyNumber = get_field('jersey_number'); if($jerseyNumber && $jerseyNumber != "PP"): ?>
	        			<span class="jersey-number">#<?php echo get_field('jersey_number'); ?></span>
	        		<?php endif; ?>
	        		<?php if(get_field('title')): ?>
	        			<span class="title"><?php echo get_field('title'); ?></span>
	        		<?php endif; ?>
	        		<?php if(get_field('captain')): ?>
	        			<span class="title">Captain</span>
	        		<?php endif; ?>
	        	</h1>
			</section>

			<section class="info">
				<div class="details">
		        	<?php if(get_field('birth_date')): ?>
		        		<?php $today = new DateTime(); $birth = get_field('birth_date'); $diff = $today->diff(new DateTime($birth)); ?>
		        		<p><strong>Age:</strong> <?php echo $diff->y ?></p>
		        	<?php endif; ?>

		        	<?php if(get_field('hometown')): ?>
		        		<p><strong>Hometown:</strong> <?php echo get_field('hometown'); ?></p>
		        	<?php endif; ?>

		        	<?php if(get_field('college')): ?>
		        		<p><strong>College:</strong> <?php echo get_field('college'); ?></p>
		        	<?php endif; ?>

		        	<?php if(get_field('nickname')): ?>
		        		<p><strong>Nickname:</strong> <?php echo get_field('nickname'); ?></p>
		        	<?php endif; ?>

		        	<?php if(get_field('hogwarts_house')): ?>
		        		<p><strong>Hogwarts House:</strong> <?php echo get_field('hogwarts_house'); ?></p>
		        	<?php endif; ?>
				</div>


	        	<div class="bio">
		        	<?php if(get_field('bio')): ?>
		        		<?php if(get_field('nickname')): ?>
		        			<h4>About <?php echo get_field('nickname'); ?></h4>
		        		<?php else: ?>
		        			<h4>About <?php echo get_field('first_name'); ?></h4>
		        		<?php endif; ?>
		        		<?php echo get_field('bio'); ?>
		        	<?php endif; ?>
	        	</div>				

        	</section>

			<?php
				$author = $post->ID;
				$args = array(
					'posts_per_page' => 3,
					'meta_key' => 'author',
					'meta_value' => $author
				);
				$wp_query = new WP_Query( $args );
				if ( $wp_query->have_posts() ) : ?>

					<section class="latest-blog">

						<div class="posts-header">
			        		<?php if(get_field('nickname', $author)): ?>
			        			<h4>Latest blog posts from <?php echo get_field('nickname', $author); ?></h4>
			        		<?php else: ?>
			        			<h4>Latest blog posts from <?php echo get_field('first_name', $author); ?></h4>
			        		<?php endif; ?>							
						</div>

		        		<div class="posts-wrapper">
				        	<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>          

								<article class="post latest-post">
									<a href="<?php the_permalink(); ?>">
										<div class="meta">
											<h4><?php the_time('j M Y'); ?></h4>
										</div>
										
										<div class="headline">
											<h3><?php the_title(); ?></h3>	
										</div>

						    			<div class="author">
											<?php $author = get_field('author'); if( $author ): ?>
										
												<h4>
													by <?php echo get_the_title($author->ID); ?><?php if(get_field('jersey_number', $author->ID)): ?>, #<?php echo get_field('jersey_number', $author->ID); ?><?php endif; ?>
												</h4>    

											<?php endif; ?>			
						    			</div>

						    			<div class="teaser">
						    				<?php echo get_field('teaser'); ?>
						    			</div>
									</a>
								</article>
							<?php endwhile; ?>		        			
		        		</div>

				</section>

			<?php endif; wp_reset_query(); ?>

        </div>					
	</section>


    <?php include('inc/roster-list.php'); ?>

<?php get_footer(); ?>