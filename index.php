<?php get_header(); ?>


	<section class="page-header">
		<div class="wrapper">

			<h1>Blog</h1>

		</div>
	</section>

	<section id="posts">
		<div class="wrapper">
		
			<div class="posts-wrapper">

				<section class="featured">
					<div class="section-header">
						<h2>Our Favorite Posts</h2>
					</div>

					<div class="featured-posts">
						<?php $page_for_posts = get_option('page_for_posts'); $posts = get_field('featured_posts', $page_for_posts); if( $posts ): ?>
							<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>

								<article class="post featured-post">
									<div class="photo">
										<a href="<?php echo get_permalink( $p->ID ); ?>">
											<img src="<?php $image = get_field('teaser_photo', $p->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										</a>
									</div>
									<div class="info">
										<div class="meta">
											<h4><?php echo get_the_time('j M Y', $p->ID); ?></h4>
										</div>

										<div class="headline">
											<h3><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h3>
										</div>

						    			<div class="author">
											<?php $author = get_field('author', $p->ID); if( $author ): ?>										
												<h4>
													by <?php echo get_the_title($author->ID); ?><?php if(get_field('jersey_number', $author->ID)): ?>, #<?php echo get_field('jersey_number', $author->ID); ?><?php endif; ?>
												</h4>
											<?php endif; ?>									
						    			</div>

						    			<div class="teaser">
						    				<?php echo get_field('teaser', $p->ID); ?>
						    			</div>											
									</div>
								</article>

							<?php endforeach; ?>
						<?php endif; ?>

					</div>


				</section>

				<section class="latest">
					<div class="section-header">
						<h2>Latest Posts</h2>
					</div>

					<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 9
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<article class="post latest-post">
							<a href="<?php the_permalink(); ?>">
								<div class="meta">
									<h4><?php the_time('j M Y'); ?></h4>
								</div>
								
								<div class="headline">
									<h3><?php the_title(); ?></h3>	
								</div>
							</a>
						</article>

					<?php endwhile; endif; wp_reset_postdata(); ?>
				</section>

			</div>

		</div>
	</section>
	
<?php get_footer(); ?>